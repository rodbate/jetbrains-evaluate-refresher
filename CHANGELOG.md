<!-- Keep a Changelog guide -> https://keepachangelog.com -->

# jetbrains-evaluate-refresher Changelog

## [Unreleased]
## [1.0.2]
### Added
- Welcome Jetbrains Evaluation Refresher!
