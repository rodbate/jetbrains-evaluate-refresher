package com.github.rodbate.jetbrains.evaluate.refresher.eval.refresher;

/**
 * Windows Evaluate Refresher
 *
 * @author rodbate
 * @since 2021/3/7
 */
public class MacEvalRefresher extends AbstractEvalRefresher {
    public static final MacEvalRefresher INSTANCE = new MacEvalRefresher();
}
